var buffer = null;
var result = "0";
var hasDot = false;
var relation = "";
var hasRelation = false;

var renderResult = () => {
  document.getElementById('eval').innerHTML = result;
}

var resetResult = () => {
  result = "0";
  renderResult();
}

var addRelation = (x) => {
  relation = x;
  if (buffer != null){
    buffer = eval(`${buffer} ${x} ${result}`);
  } else if (buffer == null) {
    buffer = Number(result);
  }
  hasRelation = true;
}

var addNumber = (x) => {
  if (hasRelation == true) {
    result = "0";
    hasRelation = false;
  }
  if(result != "0"  && x == 0) {
    result+="0";
  } else if (x >= 1 && x <= 9) {
    if (result == "0") {
      result=String(x);
    } else {
      result+=String(x);
    }
  }
  renderResult();
}

var addDot = () => {
  if (hasDot == false) {
    result+=".";
    hasDot = true;
    renderResult();
  }
}

var changeSign = () => {
  var resultNum = Number(result) * -1;
  result = String(resultNum);
  renderResult();
}

var calculate = () => {
  console.log(`${buffer} ${relation} ${result}`);
  result = eval(`${buffer} ${relation} ${result}`);
  renderResult();
  buffer = null;
  result = "0";
  hasDot = false;
  relation = "";
  hasRelation = false;
}
